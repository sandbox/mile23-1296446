<?php

/**
 * @file
 *
 * Various helpful Drupal comments wrapped up in a function.
 *
 * What is a 'knowable?' A knowable is a short statement that introduces
 * a user to a topic in a general way.
 *
 * The main text should be two sentences or less. The first sentence
 * is a broad statement regarding the topic. The second sentence explains
 * how to achieve the topic goal.
 *
 * Following these two sentences is a link to a page on drupal.org which
 * presents a more in-depth discussion of the issue. This link has two
 * components: The link target, as a relative path to drupal.org, and the
 * text that is linked.
 *
 * Example: Drupal can be extended with modules and themes. These can be
 * downloaded from drupal.org, and endabled on the Modules administration
 * page.
 *
 * Link: How to install modules and themes. (This links to
 * documentation/install/modules-themes/)
 */

/**
 * Function to gather 'knowables.'
 *
 * @return
 *  An array of knowables, each with elements that correpond to
 *  the schema.
 */
function did_you_know_knowables() {
  $knowables = array();
  $knowables[] = array(
    'knowable' => t('Drupal can be extended with modules and themes. These can be downloaded from drupal.org, and enabled on the Modules administration page.'),
    'url_text' => t('How to install modules and themes.'),
    'url' => 'documentation/install/modules-themes/',
  );
  $knowables[] = array(
    'knowable' => t('Drupal has a very active developer community, with contributors of all skill levels. There are quite a few different ways to contribute, including programming, testing, and documentation.'),
    'url_text' => t('Join in development.'),
    'url' => 'contribute/development',
  );
  $knowables[] = array(
    'knowable' => t('The Examples Project can help developers understand and use various Drupal APIs.'),
    'url_text' => t('Check out the Examples Project.'),
    'url' => 'project/examples',
  );
  return $knowables;
}
